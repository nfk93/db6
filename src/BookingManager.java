import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.time.LocalDateTime;

/**
 * Created by nfk19 on 11/10/2017.
 */
public class BookingManager {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        /**
         * ADD YOUR OWN PATH, USERNAME AND USERPASSWORD HERE!!!
         */
        Connection myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/flightsdb?characterEncoding=utf8",
                "root", "7t6vpbmr");

        // PrintWriter w = new PrintWriter(new OutputStreamWriter(System.out));
        // DriverManager.setLogWriter(w);

        String flightNR = "DEN90825";
        String bookSeatCommand = "UPDATE Seats SET isVacant=false WHERE seatNumber=? AND flightID=?";
        String seatStatusCommand = "SELECT isVacant FROM Seats WHERE seatNumber=? AND flightID=?";
        PreparedStatement getSeatStatus = myCon.prepareStatement(seatStatusCommand);
        PreparedStatement bookSeat = myCon.prepareStatement(bookSeatCommand);

        String seatNumber = "2A";
        getSeatStatus.setString(1, seatNumber);
        getSeatStatus.setString(2, flightNR);
        bookSeat.setString(1, seatNumber);
        bookSeat.setString(2, flightNR);

        seatNumber = "1A";
        getSeatStatus.setString(1, seatNumber);
        getSeatStatus.setString(2, flightNR);
        bookSeat.setString(1, seatNumber);
        bookSeat.setString(2, flightNR);

        System.out.println("READ UNCOMMITTED:");
        System.out.println("=================");

        System.out.println("Setting transaction isolation level....");
        myCon.setTransactionIsolation(1);
        System.out.println("transaction level: " + myCon.getTransactionIsolation() + "\n");

        Thread thread1 = new Thread(new RunnableQuery(getSeatStatus,bookSeat));
        Thread thread2 = new Thread(new RunnableQuery(getSeatStatus,bookSeat));

        System.out.println("Thread 1:");
        thread1.run();
        System.out.println("Thread 2:");
        thread2.run();

        System.out.println("\n");

        // Resets the database to have all sets vacant
        String resetBookingsString = "UPDATE Seats SET isVacant=true WHERE isVacant=false";
        PreparedStatement resetBookings = myCon.prepareStatement(resetBookingsString);
        resetBookings.executeUpdate();

        System.out.println("READ COMMITTED:");
        System.out.println("===============");

        System.out.println("Setting transaction isolation level....");
        myCon.setTransactionIsolation(2);
        System.out.println("transaction level: " + myCon.getTransactionIsolation() + "\n");

        Thread thread3 = new Thread(new RunnableQuery(getSeatStatus,bookSeat));
        Thread thread4 = new Thread(new RunnableQuery(getSeatStatus,bookSeat));

        System.out.println("Thread 1:");
        thread3.run();
        System.out.println("Thread 2:");
        thread4.run();

        // Resets the database to have all sets vacant
        resetBookings.executeUpdate();
    }
}

class RunnableQuery implements Runnable {
    private PreparedStatement getSeatQuery;
    private PreparedStatement bookSeatQuery;

    public RunnableQuery(PreparedStatement getSeatQuery, PreparedStatement bookSeatQuery) {
        this.getSeatQuery = getSeatQuery;
        this.bookSeatQuery = bookSeatQuery;
    }

    @Override
    public void run() {
        try {
            long time = System.nanoTime();
            System.out.println("Thread " + this.hashCode() + " - Start time: " + LocalDateTime.now());
            ResultSet seatStatus;
            seatStatus = getSeatQuery.executeQuery();
            seatStatus.next();
            boolean seatIsVacant = seatStatus.getBoolean(1);

            if (seatIsVacant) {
                bookSeatQuery.executeUpdate();
            }
            System.out.println("Thread " + this.hashCode() + " - Finish time: " + LocalDateTime.now());
            System.out.println("Thread " + this.hashCode() + " - Execution time: " + (System.nanoTime() - time));
        }
        catch (SQLException e) {
            // do nothing
        }
    }
}
